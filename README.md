# coody-verification

#### 项目介绍
一款参数自动化校验工具

#### 软件架构
没啥架构，原生java代码。基于反射+注解实现

#### 使用说明

1. 所有实体类需继承BaseModel
2. 在需要校验的字段上面标记ParamCheck注解
3. 使用ParamVerficationProcess.checkPara(BaseModel);
4. 抛出异常即为参数校验失败

ParamCheck注解如图所示

![输入图片说明](https://gitee.com/uploads/images/2018/0531/120642_d233909e_1200611.png "5.png")
#### 背景说明
本项目为Coody编写，以往在项目中嵌入使用，于2018-05-30日提取出来。

#### 功能说明
本项目支持常规参数校验、参数格式、参数可空、参数同时校验空值、层级对象校验。

使用截图：

![输入图片说明](https://gitee.com/uploads/images/2018/0531/120500_87e5c3b3_1200611.jpeg "1.jpg")
![输入图片说明](https://gitee.com/uploads/images/2018/0531/120523_e6648391_1200611.png "2.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0531/120554_107368f2_1200611.png "6+.png")


反馈邮箱：644556636@qq.com